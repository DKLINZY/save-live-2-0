import sqlite3


class SQLite:
    def __init__(self, db_name):
        self.db = sqlite3.connect(db_name, check_same_thread=False)
        self.cursor = self.db.cursor()
        self.init_database()

    def init_database(self):
        queries = [
            '''
                CREATE TABLE IF NOT EXISTS platform (
                    id integer PRIMARY KEY AUTOINCREMENT,
                    platform_name text NOT NULL,
                    platform_uri text NOT NULL
                );
        
            ''',
            '''
                CREATE TABLE IF NOT EXISTS room (
                    id integer PRIMARY KEY AUTOINCREMENT,
                    platform_id integer NOT NULL,
                    room_name text NOT NULL,
                    room_uri text NOT NULL
                );
            ''',
        ]
        for query in queries:
            self.cursor.execute(query)
        self.db.commit()

    def drop_database(self):
        tables = ['platform', 'room']
        for table in tables:
            self.cursor.execute(f'DROP TABLE IF EXISTS {table};')

    def get_table(self):
        tables = []
        query = "SELECT * FROM sqlite_master WHERE type='table';"
        data = self.cursor.execute(query)
        for row in data:
            print(row)
            tables.append(row)
        return tables

    def insert_dummy_data(self):
        queries = [
            '''
                INSERT INTO
                    platform (platform_name, platform_uri)
                VALUES
                    ("Youtube", "https://www.youtube.com/");
            ''',
            '''
                INSERT INTO 
                    room (platform_id, room_name, room_uri)
                VALUES 
                    (1, "Music", "https://www.youtube.com/watch?v=mFO8wH8bZUM"),
                    (1, "ChilledCow", "https://www.youtube.com/watch?v=5qap5aO4i9A");
            '''
        ]
        for query in queries:
            self.cursor.execute(query)
        self.db.commit()

    def get_data(self):
        _data = []
        queries = [
            'SELECT * FROM platform;',
            'SELECT * FROM room;'
        ]
        for query in queries:
            print(query)
            data = self.cursor.execute(query)
            for row in data:
                print(row)
                _data.append(row)
        return _data

    def insert_platform(self, platform_name, platform_uri):
        query = f'''
            INSERT INTO platform(platform_name, platform_uri) 
            VALUES("{platform_name}", "{platform_uri}");
        '''
        self.cursor.execute(query)
        self.db.commit()

    def insert_room(self, platform_id, room_name, room_uri):
        query = f'''
            INSERT INTO room (platform_id, room_name, room_uri) 
            VALUES ("{platform_id}", "{room_name}", "{room_uri}");
        '''
        self.cursor.execute(query)
        self.db.commit()

    def update_platform(self, platform_name, platform_uri, _id):
        query = f'''
          UPDATE platform 
          SET platform_name = "{platform_name}", 
              platform_uri = "{platform_uri}"
          WHERE id = {_id}
        '''
        self.cursor.execute(query)
        self.db.commit()

    def update_room(self, platform_id, room_name, room_uri, _id):
        query = f'''
          UPDATE room
          SET platform_id = '{platform_id}',
              room_name = '{room_name}',
              room_uri = '{room_uri}'
          WHERE id = {_id}
        '''
        self.cursor.execute(query)
        self.db.commit()

    def del_platform(self, _id):
        queries = [
            f'DELETE FROM platform WHERE id = {_id};',
            f'DELETE FROM room WHERE platform_id = {_id};'
        ]
        for query in queries:
            self.cursor.execute(query)
        self.db.commit()

    def del_room(self, _id):
        query = f'DELETE FROM room WHERE id = {_id};'
        cursor = self.db.cursor()
        cursor.execute(query)
        self.db.commit()

    def get_platform(self):
        query = 'SELECT * FROM platform'
        data = self.cursor.execute(query)
        platforms = []
        for row in data:
            platform = {
                "id": row[0],
                "platform_name": row[1],
                "platform_uri": row[2],
            }
            platforms.append(platform)
        return platforms

    def get_room(self):
        query = 'SELECT * FROM platform INNER JOIN room ON platform.id = room.platform_id'
        data = self.cursor.execute(query)
        rooms = []
        for row in data:
            room = {
                "platform_id": row[0],
                "platform_name": row[1],
                "platform_uri": row[2],
                "id": row[3],
                "room_name": row[5],
                "room_uri": row[6],
            }
            rooms.append(room)
        return rooms

    def import_query(self, queries):
        for query in queries:
            self.cursor.execute(query)
        self.db.commit()

    def export_database(self):
        export_queries = []
        get_platform_query = 'SELECT * FROM platform'
        get_room_query = 'SELECT * FROM room'

        platforms = self.cursor.execute(get_platform_query)
        for platform in platforms:
            export_queries.append(
                f'INSERT INTO platform(platform_name, platform_uri) VALUES("{platform[1]}", "{platform[2]}");'
            )

        rooms = self.cursor.execute(get_room_query)
        for room in rooms:
            export_queries.append(
                f'INSERT INTO room (platform_id, room_name, room_uri) VALUES ("{room[1]}", "{room[2]}", "{room[3]}");'
            )

        return export_queries


# sqlite = SQLite('../temp.db')
# sqlite.init_database()
# sqlite.drop_database()
# sqlite.get_table()

# sqlite.insert_dummy_data()

# sqlite.insert_platform('test', 'www.test.com')
# sqlite.update_platform('test', 'https://www.test.com', 2)

# sqlite.insert_room(2, 'testing')
# sqlite.update_room(1, 'test', 3)

# sqlite.del_platform(2)
# sqlite.del_room(4)

# sqlite.get_data()
# print(sqlite.get_platform())
# print(sqlite.get_room())

# print(sqlite.export_database())
# sqlite.import_query([])