import React from "react";
import { Dimensions } from "react-native";
import { Video } from "expo-av";

const { width } = Dimensions.get("window");

const VideoPlayer = ({ uri }) => {
  return (
    <>
      <Video
        source={{
          uri,
        }}
        rate={1.0}
        volume={1.0}
        isMuted={false}
        resizeMode={Video.RESIZE_MODE_CONTAIN}
        shouldPlay={false}
        isLooping={false}
        useNativeControls
        style={{
          margin: 10,
          width: width * 0.9,
          height: (width / 16) * 9 * 0.9,
        }}
      />
    </>
  );
};
export default React.memo(VideoPlayer, (prevProps, nextProps) => {
  return prevProps.uri === nextProps.uri;
});
