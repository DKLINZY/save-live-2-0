class StatusList:
    def __init__(self, rooms):
        self.status_list = {}
        self.refresh_status(rooms)

    def refresh_status(self, rooms):
        for room in rooms:
            room_name = room['room_name']
            room_status = {}
            room_status['platform'] = room['platform_name']
            room_status['uri'] = room['room_uri']
            room_status['isOnline'] = False
            room_status['isLoading'] = False
            room_status['isRecording'] = False
            self.status_list[room_name] = room_status

    def edit_status(self, room_name, current_status):
        self.status_list[room_name] = current_status

    def del_status(self, room_name):
        del self.status_list[room_name]

    def get_status(self, room_name):
        return self.status_list[room_name]

    def get_status_list(self):
        return self.status_list
