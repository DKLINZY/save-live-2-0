import React, { useState, useContext, useEffect } from "react";
import {
  StyleSheet,
  Alert,
  SafeAreaView,
  ScrollView,
  Keyboard,
  View,
} from "react-native";
import { Button, Text } from "react-native-elements";
import SettingsHeader from "./SettingsHeader";
import ConfigPaper from "./ConfigPaper";
import DataTransfer from "./DataTransfer";
import DebugPaper from "./DebugPaper";

import { StoreContext, UPDATE_SETTING, socketio } from "../../store";

export default function SettingsScreen() {
  const { store, dispatch } = useContext(StoreContext);
  const [serverUri, setServerUri] = useState(store.server_uri);
  const [videoUri, setVideoUri] = useState(store.video_uri);
  const [username, setUsername] = useState(store.username);
  const [password, setPassword] = useState(store.password);

  const [showTransfer, setShowTransfer] = useState(false);
  const [transferData, setTransferData] = useState("");
  // console.log(`SettingScreen | server_uri:${server_uri}`);

  const saveSetting = async () => {
    if (serverUri === "") {
      alert("Server Address is empty");
    } else if (username === "") {
      alert("Username is empty");
    } else if (password === "") {
      alert("Password is empty");
    } else {
      dispatch({
        type: UPDATE_SETTING,
        store: Object.assign({}, store, {
          server_uri: serverUri,
          video_uri: videoUri,
          username: username,
          password: password,
        }),
      });
      alert(`Setting Saved`);
      Keyboard.dismiss();
    }
  };

  const debug = (type) => {
    socketio.emit("debug", { ...store.socket_auth, action: type });
  };

  const action = (type) => {
    switch (type) {
      case "INITTABLES":
        debug(type);
        break;
      case "DROPTABLES":
        Alert.alert(
          "DROP ALL TABLES",
          "All Tables and Data Will be DELETE.",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "OK",
              onPress: () => {
                debug(type);
              },
            },
          ],
          { cancelable: false }
        );
        break;
      case "GETTABLES":
        debug(type);
        break;
      case "INSERTDUMMY":
        Alert.alert(
          "Insert Dummy Data",
          "Some Dummy Data will be Insert to Related Tables.",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "OK",
              onPress: () => {
                debug(type);
              },
            },
          ],
          { cancelable: false }
        );
        break;
      case "GETDATA":
        debug(type);
        break;
    }
  };

  const dataTransfer = (action) => {
    const send_data = { ...store.socket_auth, action };
    if (action == "IMPORT") {
      send_data.data = JSON.parse(transferData);
      setTransferData("");
    }
    // console.log(send_data);
    socketio.emit("data_transfer", send_data);
  };

  useEffect(() => {
    if (socketio) {
      socketio.on("tables", ({ tables }) => {
        console.log(tables);
      });
      socketio.on("data", ({ data }) => {
        console.log(data);
      });
      socketio.on("message", ({ title, text }) => {
        Alert.alert(title, text);
      });
      socketio.on("transfer", (data) => {
        console.log(data);
        setTransferData(JSON.stringify(data));
      });
    }
  }, []);

  return (
    <>
      <SettingsHeader saveSetting={saveSetting} />
      <ScrollView style={styles.container}>
        <ConfigPaper
          serverUri={serverUri}
          setServerUri={setServerUri}
          videoUri={videoUri}
          setVideoUri={setVideoUri}
          username={username}
          setUsername={setUsername}
          password={password}
          setPassword={setPassword}
        />
        <View style={styles.transferContainer}>
          <DataTransfer
            showTransfer={showTransfer}
            setShowTransfer={setShowTransfer}
            transferData={transferData}
            setTransferData={setTransferData}
            dataTransfer={dataTransfer}
          />
          <Text style={styles.title}>Data Transfer</Text>
          <View style={styles.button}>
            <Button
              title={"Transfer"}
              onPress={() => {
                console.log("data transfer");
                setShowTransfer(true);
              }}
            />
          </View>
        </View>
        <DebugPaper action={action} />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  transferContainer: {
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    marginVertical: 10,
  },
  button: {
    marginVertical: 5,
  },
});
