import React from "react";
import { StyleSheet, Modal, View, Dimensions } from "react-native";
import { Button, Text } from "react-native-elements";
import { Video } from "expo-av";

const { width } = Dimensions.get("window");

const PreviewVideo = ({ showPreview, setShowPreview, previewConfig }) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={showPreview}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View style={styles.container}>
        <View style={{ ...styles.modalView, width: "95%" }}>
          <Button
            style={styles.closeButton}
            type="clear"
            icon={{ name: "close", size: 20 }}
            onPress={() => {
              setShowPreview(false);
            }}
          />
          <Text
            style={{
              fontSize: 20,
              textAlign: "center",
              marginVertical: 10,
            }}
          >
            {previewConfig.name}
          </Text>
          <Video
            source={{
              uri: previewConfig.uri,
            }}
            rate={1.0}
            volume={1.0}
            isMuted={false}
            resizeMode={Video.RESIZE_MODE_CONTAIN}
            shouldPlay={false}
            isLooping={false}
            useNativeControls
            style={{
              alignSelf: "center",
              width: width * 0.95 * 0.95,
              height: (width / 16) * 9 * 0.95 * 0.95,
            }}
          />
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 5,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default React.memo(PreviewVideo, (prevProps, nextProps) => {
  return (
    prevProps.showPreview === nextProps.showPreview &&
    prevProps.previewConfig === nextProps.previewConfig
  );
});
