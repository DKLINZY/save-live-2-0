import os
import subprocess
import youtube_dl
import time
import datetime
import humanize


class Utils:
    def __init__(self, folder_name):
        self.task_list = {}
        self.file_path = os.path.join(os.getcwd(), folder_name)

    def create_folder_if_not_exists(self):
        if not (os.path.exists(self.file_path)):
            print(f'create folder: {self.file_path}')
            os.makedirs(self.file_path)

    def extract_info(self, uri):
        result = {
            'isActive': False,
            'result': {}
        }
        dl = youtube_dl.YoutubeDL({})
        try:
            _info = dl.extract_info(uri, download=False)
            result['isActive'] = True
            result['result'] = _info
        except Exception as e:
            print(e)
        finally:
            return result

    def extract_link(self, uri):
        result = {
            'isActive': False,
            'link': ''
        }
        _info = self.extract_info(uri)
        if _info['isActive']:
            result['isActive'] = True
            result['link'] = _info['result']['url']
        return result

    def record_from_link(self, uri, room_name):
        result = {
            'isActive': False,
            'downloading': False
        }

        task_is_running = False

        if room_name in self.task_list:
            if self.task_list[room_name] is not None:
                task_is_running = True
                result['error'] = f'{room_name} already exist'

        if not task_is_running:
            _link = self.extract_link(uri)
            if _link['isActive']:
                time.sleep(5)
                source = _link['link']
                now = datetime.datetime.now()
                file_name = f'{room_name}_{now.strftime("%Y%m%d%H%M%S")}.mp4'
                # file_name = f'temp_{now.strftime("%Y%m%d%H%M%S")}.mp4'
                output = os.path.join(self.file_path, file_name)

                # print(output)
                cmd = f'ffmpeg -i {source} -c:v copy -c:a copy -bsf:a aac_adtstoasc {output}'
                print(cmd)
                self.task_list[room_name] = subprocess.Popen(
                    cmd,
                    shell=True,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,

                )
                result['isActive'] = True
                result['downloading'] = True
        result['tasks'] = self.get_running_tasks()
        return result

    def get_running_tasks(self):
        result = {}
        for task in self.task_list:
            print(task)
            if self.task_list[task] is None:
                result[task] = False
            else:
                if self.task_list[task].poll() is not None:
                    self.task_list[task] = None
                    result[task] = False
                else:
                    result[task] = True
        return result

    def kill_task(self, room_name):
        result = {}
        try:
            if self.task_list[room_name] is None:
                raise Exception()
            self.task_list[room_name].terminate()
            time.sleep(5)
            self.task_list[room_name].kill()
            self.task_list[room_name] = None
            result['killed'] = True
            result['msg'] = f'{room_name} killed'
        except Exception as err:
            print(err)
            result['killed'] = False
            result['error'] = f'{room_name} is not recording'
        finally:
            result['tasks'] = self.get_running_tasks()
            return result

    def get_video_list(self):
        print('get video list')
        result = []
        video_list = os.listdir(self.file_path)
        video_list = [name for name in video_list if not name.startswith('.')]
        if video_list:
            video_list = sorted(video_list, reverse=True, key=lambda v: os.path.getmtime(os.path.join(self.file_path, v)))
        print(video_list)
        for video in video_list:
            video_info = {}
            video_path = os.path.join(self.file_path, video)
            stat = os.stat(video_path)
            print(video, stat)
            video_info['name'] = video
            video_info['createAt'] = datetime.datetime.fromtimestamp(stat.st_ctime).strftime('%Y-%m-%d %H:%M:%S')
            video_info['size'] = humanize.naturalsize(stat.st_size)
            result.append(video_info)
        return result

    def delete_video(self, file_name):
        video_name = os.path.join(self.file_path, file_name)
        os.remove(video_name)
