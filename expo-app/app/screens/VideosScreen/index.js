import React, { useContext, useState, useEffect, useReducer } from "react";
import { StyleSheet, View, ScrollView, Alert } from "react-native";
import VideosHeader from "./VideosHeader";
import VideoListItem from "./VideoListItem";
import PreviewVideo from "../Public/PreviewVideo";
import { StoreContext, socketio } from "../../store";

export default function VideosScreen() {
  const { store } = useContext(StoreContext);

  const [showPreview, setShowPreview] = useState(false);
  const [refreshingVideos, setRefreshingVideos] = useState(false);

  const [videoList, setVideoList] = useState([]);
  const [previewConfig, setPreviewConfig] = useState({ name: "", uri: "" });

  const refreshVideos = () => {
    console.log("refresh videos list");
    setRefreshingVideos(true);
    socketio.emit("videos", { ...store.socket_auth, action: "GET" });
  };

  const selectVideo = (name) => {
    // console.log(name:name,uri:`${store.video_uri}/${name}`);
    setPreviewConfig({ name: name, uri: `${store.video_uri}/${name}` });
    setShowPreview(true);
  };

  const deleteVideo = (name) => {
    console.log(`delete ${name}`);
    Alert.alert(
      "Delete Video",
      `Delete ${name}?`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            socketio.emit("videos", {
              ...store.socket_auth,
              action: "DELETE",
              file_name: name,
            });
          },
        },
      ],
      { cancelable: false }
    );
  };

  useEffect(() => {
    if (socketio) {
      socketio.on("video_list", ({ videos }) => {
        console.log(videos);
        setRefreshingVideos(false);
        setVideoList(videos);
      });

      refreshVideos();
    }
  }, []);

  return (
    <>
      <VideosHeader
        refreshingVideos={refreshingVideos}
        refreshVideos={refreshVideos}
      />
      <View style={styles.container}>
        <PreviewVideo
          showPreview={showPreview}
          setShowPreview={setShowPreview}
          previewConfig={previewConfig}
        />
        <ScrollView style={styles.videoListView}>
          <VideoListItem
            videoList={videoList}
            selectVideo={selectVideo}
            deleteVideo={deleteVideo}
          />
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
  },
  videoListView: {
    width: "100%",
  },
});
