import React, { createContext, useReducer, useEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import io from "socket.io-client";

export const StoreContext = createContext({});

export const INIT_SETTING = "INIT_SETTING";
export const UPDATE_SETTING = "UPDATE_SETTING";

export let socketio = null;

const connectSocketIO = (uri) => {
  // socketio = io(`${uri}/socket.io`, { transports: ["websocket"] });
  socketio = io(uri, {
    reconnectionDelay: 1000,
    reconnection: true,
    reconnectionAttempts: 10,
    transports: ["websocket"],
    agent: false,
    upgrade: false,
    rejectUnauthorized: false,
  });
};

const reducer = (state, action) => {
  switch (action.type) {
    case INIT_SETTING:
      console.log(`INIT_SETTING:${JSON.stringify(action.store)}`);
      connectSocketIO(action.store.server_uri);
      return action.store;
    case UPDATE_SETTING:
      console.log(`UPDATE_SETTING:${JSON.stringify(action.store)}`);
      AsyncStorage.setItem("config", JSON.stringify(action.store));
      connectSocketIO(action.store.server_uri);
      return action.store;
    default:
      return state;
  }
};

export const Store = (porps) => {
  const [store, dispatch] = useReducer(reducer, {});
  return (
    <StoreContext.Provider value={{ store, dispatch }}>
      {porps.children}
    </StoreContext.Provider>
  );
};
