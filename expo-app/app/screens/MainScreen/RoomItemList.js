import React, { useMemo } from "react";
import { StyleSheet, View, Switch } from "react-native";
import { Button, Text } from "react-native-elements";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const RoomItemList = ({ statusList, refreshStatus, record, livePreview }) => {
  return (
    <>
      {Object.keys(statusList).map((key, index) => {
        return (
          <View key={index + key} style={styles.roomItem}>
            <View style={styles.roomHeader}>
              <View style={{ ...styles.gridBorder, flex: 2 }}>
                <Text
                  style={{
                    fontSize: 20,
                    textAlign: "center",
                  }}
                >
                  {statusList[key].platform}
                </Text>
              </View>
              <View style={{ ...styles.gridBorder, flex: 3 }}>
                <Text
                  style={{
                    fontSize: 20,
                    textAlign: "center",
                  }}
                >
                  {key}
                </Text>
              </View>
            </View>
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                marginVertical: 10,
              }}
            >
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {statusList[key].isOnline ? (
                  <MaterialCommunityIcons
                    name="emoticon-happy-outline"
                    color="darkcyan"
                    size={30}
                  />
                ) : (
                  <MaterialCommunityIcons
                    name="emoticon-sad-outline"
                    color="brown"
                    size={30}
                  />
                )}
              </View>
              <View style={{ ...styles.gridBorder, flex: 1 }}>
                <Button
                  icon={{ name: "refresh", color: "#737373", size: 30 }}
                  type="clear"
                  loading={statusList[key].isLoading}
                  onPress={() => {
                    refreshStatus(key, statusList[key].uri);
                  }}
                />
              </View>
              <View style={{ ...styles.gridBorder, flex: 1 }}>
                <Button
                  icon={{
                    name: "play-circle-outline",
                    color: "#737373",
                    size: 30,
                  }}
                  type="clear"
                  onPress={() => {
                    livePreview(key, statusList[key].uri);
                  }}
                />
              </View>
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Switch
                  trackColor={{ false: "#767577", true: "#75D1FF" }}
                  thumbColor={"#FFF"}
                  onValueChange={() => {
                    record(key, statusList[key].uri);
                  }}
                  value={statusList[key].isRecording}
                />
              </View>
            </View>
          </View>
        );
      })}
    </>
  );
};

const styles = StyleSheet.create({
  roomItem: {
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  roomHeader: {
    display: "flex",
    flexDirection: "row",
  },

  gridBorder: {
    backgroundColor: "#EFEFEF",
    borderRadius: 20,
    paddingVertical: 5,
    marginHorizontal: 5,
  },
});

export default React.memo(RoomItemList, (prevProps, nextProps) => {
  return prevProps.statusList === nextProps.statusList;
});
