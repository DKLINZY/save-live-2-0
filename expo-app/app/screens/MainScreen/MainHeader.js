import React from "react";
import { StyleSheet } from "react-native";
import { Header, Text, Button } from "react-native-elements";

const MainHeader = ({ refreshingAll, refreshAllStatus, setShowOptionList }) => {
  // console.log("MainHeader");
  return (
    <Header
      statusBarProps={{ translucent: true }}
      leftComponent={
        <Button
          type="clear"
          icon={{ name: "refresh", color: "white" }}
          loading={refreshingAll}
          onPress={refreshAllStatus}
        />
      }
      centerComponent={<Text style={styles.headerText}>SaveLive</Text>}
      rightComponent={
        <Button
          type="clear"
          icon={{ name: "add", color: "white" }}
          onPress={() => {
            setShowOptionList(true);
          }}
        />
      }
      containerStyle={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    color: "#FFFFFF",
  },
  container: {
    backgroundColor: "#7A7A7A",
  },
});

export default React.memo(MainHeader, (prevProps, nextProps) => {
  return prevProps.refreshingAll === nextProps.refreshingAll;
});
