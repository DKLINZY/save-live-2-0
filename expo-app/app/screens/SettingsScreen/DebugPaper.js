import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, Text } from "react-native-elements";

const DebugPaper = ({ action }) => {
  return (
    <View style={styles.serverSettingPaper}>
      <Text style={styles.title}>Database Debug</Text>
      <View style={styles.button}>
        <Button
          title={"Init Tables"}
          onPress={() => {
            action("INITTABLES");
          }}
        />
      </View>
      <View style={styles.button}>
        <Button
          title={"Drop all Tables"}
          onPress={() => {
            action("DROPTABLES");
          }}
        />
      </View>
      <View style={styles.button}>
        <Button
          title={"Get Tables Info"}
          onPress={() => {
            action("GETTABLES");
          }}
        />
      </View>
      <View style={styles.button}>
        <Button
          title={"Insert Dummy Data"}
          onPress={() => {
            action("INSERTDUMMY");
          }}
        />
      </View>
      <View style={styles.button}>
        <Button
          title={"Get Data"}
          onPress={() => {
            action("GETDATA");
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  serverSettingPaper: {
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    marginVertical: 10,
  },
  button: {
    marginVertical: 5,
  },
});

export default React.memo(DebugPaper);
