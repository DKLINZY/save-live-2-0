import React from "react";
import { StyleSheet, View } from "react-native";
import { Input } from "react-native-elements";

const ConfigPaper = ({
  serverUri,
  setServerUri,
  videoUri,
  setVideoUri,
  username,
  setUsername,
  password,
  setPassword,
}) => {
  return (
    <View style={styles.serverSettingPaper}>
      <Input
        label={"API Server Address"}
        value={serverUri}
        onChangeText={(value) => {
          setServerUri(value);
        }}
      />
      <Input
        label={"Video Server Address"}
        value={videoUri}
        onChangeText={(value) => {
          setVideoUri(value);
        }}
      />
      <Input
        label={"Username"}
        value={username}
        onChangeText={(value) => {
          setUsername(value);
        }}
      />
      <Input
        label={"Password"}
        secureTextEntry={true}
        value={password}
        onChangeText={(value) => {
          setPassword(value);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  serverSettingPaper: {
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
});

export default React.memo(ConfigPaper, (prevProps, nextProps) => {
  return (
    prevProps.serverUri === nextProps.serverUri &&
    prevProps.videoUri === nextProps.videoUri &&
    prevProps.username === nextProps.username &&
    prevProps.password === nextProps.password
  );
});
