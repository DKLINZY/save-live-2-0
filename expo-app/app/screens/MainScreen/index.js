import React, { useState, useEffect, useContext, useReducer } from "react";
import { StyleSheet, View, ScrollView, Alert, Keyboard } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

import { StoreContext, socketio, INIT_SETTING } from "../../store";

import MainHeader from "./MainHeader";
import OptionList from "./OptionList";
import PlatformList from "./PlatformList";
import RoomList from "./RoomList";
import RoomItemList from "./RoomItemList";
import PreviewVideo from "../Public/PreviewVideo";

export default function MainScreen() {
  const [showOptionList, setShowOptionList] = useState(false);
  const [showPlatformList, setShowPlatformList] = useState(false);
  const [showRoomList, setShowRoomList] = useState(false);
  const [showPreview, setShowPreview] = useState(false);

  const [refreshingAll, setRefreshingAll] = useState(false);

  const [platformList, setPlatformList] = useState([]);
  const [roomList, setRoomList] = useState([]);
  const [statusList, setStatusList] = useState({});
  const [previewConfig, setPreviewConfig] = useState({ name: "", uri: "" });

  const { store, dispatch } = useContext(StoreContext);

  useEffect(() => {
    const getConfig = async () => {
      try {
        const config = await AsyncStorage.getItem("config");
        return config ? JSON.parse(config) : {};
      } catch (e) {
        console.log("Failed to fetch the data from storage");
      }
    };

    const fetchData = async () => {
      const config = await getConfig();
      // console.log(config);
      dispatch({
        type: INIT_SETTING,
        store: {
          server_uri: "server_uri" in config ? config.server_uri : "",
          video_uri: "video_uri" in config ? config.video_uri : "",
          username: "username" in config ? config.username : "",
          password: "password" in config ? config.password : "",
          socket_auth: {
            username: "username" in config ? config.username : "",
            password: "password" in config ? config.password : "",
          },
        },
      });
    };

    fetchData();
  }, []);

  useEffect(() => {
    console.log("socketio change", store);
    if (socketio) {
      socketio.on("connect", () => {
        console.log("SocketIO-connected");
        socketio.emit("platform", { ...store.socket_auth, action: "GET" });
        socketio.emit("room", { ...store.socket_auth, action: "GET" });
        socketio.emit("get_status", { ...store.socket_auth });
      });

      socketio.on("disconnect", () => {
        console.log("SocketIO-disconnect");
      });

      socketio.on("platforms", ({ platforms }) => {
        console.log("SocketIO-platforms", platforms);
        setPlatformList(transPlatformFormat(platforms));
      });

      socketio.on("rooms", ({ rooms }) => {
        console.log("SocketIO-rooms", rooms);
        setRoomList(rooms);
      });

      socketio.on("status", (status) => {
        console.log("SocketIO-status", status);
        setStatusList(status);
      });

      socketio.on("live_preview", ({ config }) => {
        // console.log(config);
        setPreviewConfig(config);
        setShowPreview(true);
      });

      socketio.on("error", (error) => {
        console.log(error);
        if ("error" in error) Alert.alert("ERROR", error.error);
      });

      socketio.on("end_refresh_all", () => {
        setRefreshingAll(false);
      });
    }
  }, [socketio]);

  //functions

  const refreshAllStatus = () => {
    console.log("refresh all status");
    socketio.emit("refresh_all", {
      ...store.socket_auth,
    });
    setRefreshingAll(true);
  };

  const refreshStatus = (room_name, room_uri) => {
    console.log("refresh status");
    socketio.emit("refresh", {
      ...store.socket_auth,
      room_name,
      uri: room_uri,
    });
    let _statusList = { ...statusList };
    _statusList[room_name].isLoading = true;
    // console.log(_statusList);
    setStatusList(_statusList);
  };

  const livePreview = (room_name, room_uri) => {
    socketio.emit("live", { ...store.socket_auth, room_name, room_uri });
  };

  const record = (room_name, room_uri) => {
    let send_data = { ...store.socket_auth };
    if (!statusList[room_name].isRecording) {
      send_data.action = "SAVE";
      send_data.room_name = room_name;
      send_data.uri = room_uri;
    } else {
      send_data.action = "KILL";
      send_data.room_name = room_name;
    }
    // console.log(send_data);
    socketio.emit("record", send_data);
  };

  const transPlatformFormat = (paltforms) => {
    let transPaltforms = [];
    for (let platform of paltforms) {
      const _platform = {
        id: platform.id,
        label: platform.platform_name,
        value: platform.platform_uri,
      };
      transPaltforms.push(_platform);
    }
    return transPaltforms;
  };

  // save/delete platform and room

  const savePlatform = (newPlatform) => {
    if (newPlatform.label === "" || newPlatform.value === "") {
      alert(`Platform Name or Address Can't be Empty`);
    } else {
      // console.log(newPlatform);
      let send_data = { ...store.socket_auth };
      if (newPlatform.id === 0) {
        send_data.action = "ADD";
        send_data.platform_name = newPlatform.label;
        send_data.platform_uri = newPlatform.value;
      } else {
        send_data.action = "UPDATE";
        send_data.id = newPlatform.id;
        send_data.platform_name = newPlatform.label;
        send_data.platform_uri = newPlatform.value;
      }
      // console.log(send_data);
      socketio.emit("platform", send_data);
      Keyboard.dismiss();
    }
  };

  const deletePlatform = (id, label) => {
    Alert.alert(
      "Delete Platform",
      `Delete Platfrom ${label} and it's Rooms?`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            socketio.emit("platform", {
              ...store.socket_auth,
              action: "DELETE",
              id,
            });
          },
        },
      ],
      { cancelable: false }
    );
  };

  const saveRoom = (newRoom) => {
    // console.log("save room");
    // console.log(newRoom);
    if (
      newRoom.platform_id === 0 ||
      newRoom.room_name === "" ||
      newRoom.room_uri === ""
    ) {
      alert("Platform id, Room name or Room uri Can't be Empty");
    } else {
      let send_data = { ...store.socket_auth };
      if (newRoom.id === 0) {
        send_data.action = "ADD";
        send_data.platform_id = newRoom.platform_id;
        send_data.room_name = newRoom.room_name;
        send_data.room_uri = newRoom.room_uri;
      } else {
        send_data.action = "UPDATE";
        send_data.id = newRoom.id;
        send_data.platform_id = newRoom.platform_id;
        send_data.room_name = newRoom.room_name;
        send_data.room_uri = newRoom.room_uri;
      }
      socketio.emit("room", send_data);
      Keyboard.dismiss();
    }
  };

  const deleteRoom = (id, room_name) => {
    Alert.alert(
      "Delete Room",
      `Delete Room ${room_name}?`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            socketio.emit("room", {
              ...store.socket_auth,
              action: "DELETE",
              id,
            });
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <>
      <MainHeader
        refreshingAll={refreshingAll}
        refreshAllStatus={refreshAllStatus}
        setShowOptionList={setShowOptionList}
      />

      <PreviewVideo
        showPreview={showPreview}
        setShowPreview={setShowPreview}
        previewConfig={previewConfig}
      />

      <OptionList
        showOptionList={showOptionList}
        setShowOptionList={setShowOptionList}
        setShowPlatformList={setShowPlatformList}
        setShowRoomList={setShowRoomList}
      />
      <PlatformList
        platformList={platformList}
        showPlatformList={showPlatformList}
        setShowPlatformList={setShowPlatformList}
        deletePlatform={deletePlatform}
        savePlatform={savePlatform}
      />
      <RoomList
        platformList={platformList}
        roomList={roomList}
        showRoomList={showRoomList}
        setShowRoomList={setShowRoomList}
        deleteRoom={deleteRoom}
        saveRoom={saveRoom}
      />

      <View style={styles.container}>
        <ScrollView>
          <RoomItemList
            statusList={statusList}
            refreshStatus={refreshStatus}
            record={record}
            livePreview={livePreview}
          />
        </ScrollView>
      </View>
    </>
  );
}

//style
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
