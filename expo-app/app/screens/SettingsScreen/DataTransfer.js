import React, { useState } from "react";
import { StyleSheet, View, Modal } from "react-native";
import { Button, Text, Input } from "react-native-elements";

const DataTransfer = ({
  showTransfer,
  setShowTransfer,
  transferData,
  setTransferData,
  dataTransfer,
}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={showTransfer}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View style={styles.container}>
        <View style={{ ...styles.modalView, width: "95%" }}>
          <Button
            style={styles.closeButton}
            type="clear"
            icon={{ name: "close", size: 20 }}
            onPress={() => {
              setShowTransfer(false);
            }}
          />
          <Text style={styles.title}>{"Data Transfer"}</Text>
          <View>
            <Input
              value={transferData}
              onChangeText={(value) => {
                // console.log(value);
                setTransferData(value);
              }}
            />
          </View>
          <View style={styles.buttonView}>
            <View style={styles.button}>
              <Button
                title={"Export Data"}
                onPress={() => {
                  dataTransfer("EXPORT");
                }}
              />
            </View>
            <View style={styles.button}>
              <Button
                title={"Import Data"}
                onPress={() => {
                  dataTransfer("IMPORT");
                }}
              />
            </View>
            <View style={styles.button}>
              <Button
                title={"Clear"}
                onPress={() => {
                  setTransferData("");
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    marginVertical: 10,
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 5,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonView: {
    display: "flex",
    flexDirection: "row",
    marginHorizontal: 20,
  },
  button: {
    flex: 1,
    marginHorizontal: 10,
  },
});

export default React.memo(DataTransfer, (prevProps, nextProps) => {
  return (
    prevProps.showTransfer === nextProps.showTransfer &&
    prevProps.transferData === nextProps.transferData
  );
});
