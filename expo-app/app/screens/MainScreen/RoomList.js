import React, { useState } from "react";
import { StyleSheet, View, Modal, ScrollView } from "react-native";
import { Button, Input, ListItem, Text } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";

const RoomList = ({
  roomList,
  platformList,
  showRoomList,
  setShowRoomList,
  deleteRoom,
  saveRoom,
}) => {
  const [newRoom, setNewRoom] = useState({
    id: 0,
    platform_id: 0,
    platform_value: "",
    room_name: "",
    room_uri: "",
  });

  const roomModalValueChange = (type, value) => {
    console.log(type, value);
    let _newRoom = { ...newRoom };
    if (type === "platform_value") {
      _newRoom["platform_id"] = value ? platformList[value - 1].id : 0;
      _newRoom["platform_value"] = value ? platformList[value - 1].value : "";
    } else {
      _newRoom[type] = value;
    }
    setNewRoom(_newRoom);
  };

  const editRoom = (id, platform_id, platform_value, room_name, room_uri) => {
    console.log(id, platform_id, platform_value, room_name, room_uri);
    setNewRoom({ id, platform_id, platform_value, room_name, room_uri });
  };

  const clearRoom = () =>
    setNewRoom({
      id: 0,
      platform_id: 0,
      platform_value: "",
      room_name: "",
      room_uri: "",
    });

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={showRoomList}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View style={styles.container}>
        <View style={styles.modalView}>
          <Button
            style={styles.closeButton}
            type="clear"
            icon={{ name: "close", size: 20 }}
            onPress={() => {
              setShowRoomList(false);
            }}
          />

          <RNPickerSelect
            items={platformList}
            value={newRoom.platform_value}
            onValueChange={(_, index) => {
              roomModalValueChange("platform_value", index);
            }}
            style={pickerSelectStyles}
          />

          <Input
            label={"Room Name"}
            value={newRoom.room_name}
            onChangeText={(value) => {
              roomModalValueChange("room_name", value);
            }}
          />
          <Input
            label={"Room URI"}
            value={newRoom.room_uri}
            onChangeText={(value) => {
              roomModalValueChange("room_uri", value);
            }}
          />
          <View style={styles.saveView}>
            <Button
              style={styles.modalButton}
              type="clear"
              title="Clear"
              onPress={clearRoom}
            />
            <Button
              style={styles.modalButton}
              type="clear"
              title="Save"
              onPress={() => {
                saveRoom(newRoom);
                clearRoom();
              }}
            />
          </View>

          <Text style={styles.titleText}>Room</Text>
          <ScrollView style={styles.scrollView}>
            {roomList.map((item, index) => (
              <ListItem
                key={index}
                title={item.room_name}
                subtitle={item.room_uri}
                // delayLongPress={2000} //for enable remote debugging
                onPress={() =>
                  editRoom(
                    item.id,
                    item.platform_id,
                    item.platform_uri,
                    item.room_name,
                    item.room_uri
                  )
                }
                onLongPress={() => {
                  deleteRoom(item.id, item.room_name);
                  clearRoom();
                }}
                bottomDivider
              />
            ))}
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    width: "90%",
    height: "80%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 5,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButton: {
    margin: 5,
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  saveView: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  titleText: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 30,
  },
  scrollView: {
    height: 300,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    margin: 10,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    color: "black",
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    margin: 10,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "purple",
    borderRadius: 8,
    color: "black",
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

export default React.memo(RoomList, (prevProps, nextProps) => {
  return (
    prevProps.roomList === nextProps.roomList &&
    prevProps.platformList === nextProps.platformList &&
    prevProps.showRoomList === nextProps.showRoomList
  );
});
