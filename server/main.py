from modules.utils import Utils
from modules.database import SQLite
from modules.status import StatusList

from flask import Flask, request, make_response, jsonify
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash
from flask_socketio import SocketIO, emit

import json

app = Flask(__name__)
app.config['SECRET_KEY'] = '6aa9333a-9140-4d91-b313-bec3775a1adc'
socketio = SocketIO(app, cors_allowed_origins="*", cookie=False)
auth = HTTPBasicAuth()

config_json_path = 'app_data/config.json'
config_json = json.load(open(config_json_path))
users = config_json['users']

utils = Utils('app_data/videos')
sqlite = SQLite('app_data/database.db')

socket_path = '/socket.io'
status_list = StatusList(sqlite.get_room())


def authenticate(username, password):
    for user in users:
        if user['username'] == username:
            if check_password_hash(user['password'], password):
                return True
    return False


@auth.verify_password
def verify_password(username, password):
    return authenticate(username, password)


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.route('/')
@auth.login_required
def index():
    return 'Save Live Server is running!!'


@app.route('/tasks')
@auth.login_required
def tasks():
    return {'tasks': utils.get_running_tasks()}


@app.route('/videos')
@auth.login_required
def videos():
    return {'videos': utils.get_video_list()}


# -------------------------------- Version 1 ---------------------------------------

@app.route('/info')
@auth.login_required
def info():
    result = {}
    platform = request.args.get('platform')
    _id = request.args.get('id')

    if platform and _id:
        if not platform.endswith('/'):
            platform += '/'
        uri = platform + _id
        result = utils.extract_info(uri)
    else:
        result['error'] = 'platform and id are required!'

    return result


@app.route('/link')
@auth.login_required
def link():
    result = {}
    platform = request.args.get('platform')
    _id = request.args.get('id')

    if platform and _id:
        if not platform.endswith('/'):
            platform += '/'
        uri = platform + _id
        result = utils.extract_link(uri)
    else:
        result['error'] = 'platform and id are required!'

    return result


@app.route('/save')
@auth.login_required
def save():
    result = {}
    platform = request.args.get('platform')
    _id = request.args.get('id')

    if platform and _id:
        if not platform.endswith('/'):
            platform += '/'
        uri = platform + _id
        result = utils.record_from_link(uri, _id)
    else:
        result['error'] = 'platform and id are required'

    return result


@app.route('/kill')
@auth.login_required
def kill():
    result = {}
    platform = request.args.get('platform')
    _id = request.args.get('id')

    if platform and _id:
        result = utils.kill_task(_id)
    else:
        result['error'] = 'platform and id are required'

    return result

# ------------------------------ End of Version 1 ----------------------------------


# -------------------------------- Version 2 ---------------------------------------

@app.route('/v2/info')
@auth.login_required
def info_v2():
    result = {}
    uri = request.args.get('uri')

    if uri:
        result = utils.extract_info(uri)
    else:
        result['error'] = 'uri is required!'

    return result


@app.route('/v2/link')
@auth.login_required
def link_v2():
    result = {}
    uri = request.args.get('uri')

    if uri:
        result = utils.extract_link(uri)
    else:
        result['error'] = 'uri is required!'

    return result


@app.route('/v2/save')
@auth.login_required
def save_v2():
    result = {}
    room_name = request.args.get('room_name')
    uri = request.args.get('uri')

    if uri:
        result = utils.record_from_link(uri, room_name)
    else:
        result['error'] = 'uri and room_name are required'

    return result


@app.route('/v2/kill/<room_name>')
@auth.login_required
def kill_v2(room_name):
    result = {}

    if room_name:
        result = utils.kill_task(room_name)
    else:
        result['error'] = 'room_name is required'

    return result


@app.route('/v2/platform', methods=['GET', 'POST', 'PUT', 'DELETE'])
@auth.login_required
def platform_storage():
    result = {}

    if request.method not in ['GET', 'POST', 'PUT', 'DELETE']:
        result['error'] = 'The API supports GET/POST/PUT/DELETE methods'

    else:
        if request.method == 'POST':
            request_data = request.get_json()
            if 'platform_name' in request_data and 'platform_uri' in request_data:
                platform_name = request_data['platform_name']
                platform_uri = request_data['platform_uri']
                # print(platform_name, platform_uri)
                sqlite.insert_platform(platform_name, platform_uri)
                result['success'] = True
            else:
                result['success'] = False
                result['error'] = 'platform_name and platform_uri cannot be Empty'

        elif request.method == 'PUT':
            request_data = request.get_json()
            if 'platform_name' in request_data and 'platform_uri' in request_data and 'id' in request_data:
                platform_name = request_data['platform_name']
                platform_uri = request_data['platform_uri']
                _id = request_data['id']
                # print(platform_name, platform_uri, _id)
                sqlite.update_platform(platform_name, platform_uri, _id)
                result['success'] = True
            else:
                result['success'] = False
                result['error'] = 'platform_name and platform_uri and id cannot be Empty'

        elif request.method == 'DELETE':
            request_data = request.get_json()
            if 'id' in request_data:
                _id = request_data['id']
                sqlite.del_platform(_id)
                result['success'] = True
            else:
                result['success'] = False
                result['error'] = 'id cannot be Empty'

        result['platforms'] = sqlite.get_platform()

    return result


@app.route('/v2/room', methods=['GET', 'POST', 'PUT', 'DELETE'])
@auth.login_required
def room_storage():
    result = {}

    if request.method not in ['GET', 'POST', 'PUT', 'DELETE']:
        result['error'] = 'The API supports GET/POST/PUT/DELETE methods'

    else:

        if request.method == 'POST':
            request_data = request.get_json()
            if 'platform_id' in request_data and 'room_name' in request_data and 'room_uri' in request_data:
                platform_id = request_data['platform_id']
                room_name = request_data['room_name']
                room_uri = request_data['room_uri']
                # print(platform_id, room_id)
                sqlite.insert_room(platform_id, room_name, room_uri)
                result['success'] = True
            else:
                result['success'] = False
                result['error'] = 'platform_id, room_name and room_uri cannot be Empty'

        elif request.method == 'PUT':
            request_data = request.get_json()
            if 'platform_id' in request_data and 'room_name' in request_data and 'room_uri' in request_data:
                platform_id = request_data['platform_id']
                room_name = request_data['room_name']
                room_uri = request_data['room_uri']
                _id = request_data['id']
                # print(platform_id, room_id, _id)
                sqlite.update_room(platform_id, room_name, room_uri, _id)
                result['success'] = True
            else:
                result['success'] = False
                result['error'] = 'platform_id, room_name and room_uri cannot be Empty'

        elif request.method == 'DELETE':
            request_data = request.get_json()
            if 'id' in request_data:
                _id = request_data['id']
                sqlite.del_room(_id)
                result['success'] = True
            else:
                result['success'] = False
                result['error'] = 'id cannot be Empty'

        result['rooms'] = sqlite.get_room()

    return result


@app.route('/v2/debug/<action>')
@auth.login_required
def debug(action):
    print(action)
    result = {}
    try:
        result['action'] = action
        result['success'] = True
        if action == "inittables":
            sqlite.init_database()
        elif action == "droptables":
            sqlite.drop_database()
        elif action == "gettables":
            result['tables'] = sqlite.get_table()
        elif action == "insertdummy":
            sqlite.insert_dummy_data()
        elif action == "getdata":
            result['data'] = sqlite.get_data()
        else:
            result['error'] = "this action not found"
            raise Exception()
    except Exception:
        result['success'] = False

    return result


# ------------------------------ End of Version 2 ----------------------------------


# --------------------------- SocketIO ----------------------------

@socketio.on('connect', namespace=socket_path)
def socket_connect():
    print('connected')


@socketio.on('disconnect', namespace=socket_path)
def socket_disconnect():
    print('Client disconnected')


@socketio.on('refresh', namespace=socket_path)
def refresh(_auth):
    # print(_auth)
    if 'username' in _auth and 'password' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            uri = _auth['uri']
            room_name = _auth['room_name']
            result = utils.extract_info(uri)
            room_status = status_list.get_status(room_name)
            if result['isActive']:
                room_status['isOnline'] = True
            record_tasks = utils.get_running_tasks()
            if room_name in record_tasks:
                room_status['isRecording'] = record_tasks[room_name]
            status_list.edit_status(room_name, room_status)
            emit('status', status_list.get_status_list(), broadcast=True)
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})


@socketio.on('refresh_all', namespace=socket_path)
def refresh_all(_auth):
    # print(_auth)
    if 'username' in _auth and 'password' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            rooms = sqlite.get_room()
            for room in rooms:
                uri = room['room_uri']
                room_name = room['room_name']
                result = utils.extract_info(uri)
                room_status = status_list.get_status(room_name)
                if result['isActive']:
                    room_status['isOnline'] = True
                    room_status['result'] = result['result']
                record_tasks = utils.get_running_tasks()
                if room_name in record_tasks:
                    room_status['isRecording'] = record_tasks[room_name]
                status_list.edit_status(room_name, room_status)
            emit('status', status_list.get_status_list(), broadcast=True)
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})
    emit('end_refresh_all')


@socketio.on('live', namespace=socket_path)
def live(_auth):
    print(_auth)
    if 'username' in _auth and 'password' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            room_name = _auth['room_name']
            room_uri = _auth['room_uri']
            result = utils.extract_info(room_uri)
            if result['isActive']:
                emit('live_preview', {'config': {'name': room_name, 'uri': result['result']['url']}})
            else:
                emit('error', {'error', f'{room_name} is Offline'})

        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})


@socketio.on('record', namespace=socket_path)
def record(_auth):
    if 'username' in _auth and 'password' in _auth and 'action' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            if _auth['action'] == 'SAVE':
                uri = _auth['uri']
                room_name = _auth['room_name']
                result = utils.record_from_link(uri, room_name)
                if 'error' in result:
                    emit('error', {'error': result['error']})
                else:
                    if result['downloading']:
                        room_status = status_list.get_status(room_name)
                        room_status['isRecording'] = True
                        status_list.edit_status(room_name, room_status)
                        emit('status', status_list.get_status_list(), broadcast=True)
                    else:
                        emit('error', {'error': f'{room_name} is offline'})

            elif _auth['action'] == 'KILL':
                room_name = _auth['room_name']
                result = utils.kill_task(room_name)
                if 'error' in result:
                    emit('error', {'error': result['error']})
                else:
                    if result['killed']:
                        room_status = status_list.get_status(room_name)
                        room_status['isRecording'] = False
                        status_list.edit_status(room_name, room_status)
                        emit('status', status_list.get_status_list(), broadcast=True)
            else:
                emit('error', {'error': 'Wrong Action'})
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})


@socketio.on('platform', namespace=socket_path)
def platform_socketio(_auth):
    if 'username' in _auth and 'password' in _auth and 'action' in _auth:
        if not authenticate(_auth['username'], _auth['password']):
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
        else:
            if not _auth['action'] in ['GET', 'ADD', 'UPDATE', 'DELETE']:
                emit('error', {'error': 'Wrong Action'})
            else:
                if _auth['action'] == 'ADD':
                    platform_name = _auth['platform_name']
                    platform_uri = _auth['platform_uri']
                    # print(platform_name, platform_uri)
                    sqlite.insert_platform(platform_name, platform_uri)
                elif _auth['action'] == 'UPDATE':
                    platform_name = _auth['platform_name']
                    platform_uri = _auth['platform_uri']
                    _id = _auth['id']
                    # print(platform_name, platform_uri, _id)
                    sqlite.update_platform(platform_name, platform_uri, _id)
                elif _auth['action'] == 'DELETE':
                    _id = _auth['id']
                    sqlite.del_platform(_id)
                    rooms = sqlite.get_room()
                    status_list.refresh_status(rooms)
                    emit('rooms', {'rooms': rooms}, broadcast=True)
                emit('platforms', {'platforms': sqlite.get_platform()}, broadcast=True)
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username, Password and Action are required!'})


@socketio.on('room', namespace=socket_path)
def room_socketio(_auth):
    if 'username' in _auth and 'password' in _auth and 'action' in _auth:
        if not authenticate(_auth['username'], _auth['password']):
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
        else:
            if not _auth['action'] in ['GET', 'ADD', 'UPDATE', 'DELETE']:
                emit('error', {'error': 'Wrong Action'})
            else:
                if _auth['action'] == 'ADD':
                    platform_id = _auth['platform_id']
                    room_name = _auth['room_name']
                    room_uri = _auth['room_uri']
                    # print(platform_id, room_id)
                    sqlite.insert_room(platform_id, room_name, room_uri)
                elif _auth['action'] == 'UPDATE':
                    platform_id = _auth['platform_id']
                    room_name = _auth['room_name']
                    room_uri = _auth['room_uri']
                    _id = _auth['id']
                    # print(platform_id, room_id, _id)
                    sqlite.update_room(platform_id, room_name, room_uri, _id)
                elif _auth['action'] == 'DELETE':
                    _id = _auth['id']
                    sqlite.del_room(_id)
                emit('rooms', {'rooms': sqlite.get_room()}, broadcast=True)
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username, Password and Action are required!'})


@socketio.on('get_status', namespace=socket_path)
def status(_auth):
    if 'username' in _auth and 'password' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            emit('status', status_list.get_status_list())
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})


@socketio.on('data_transfer', namespace=socket_path)
def transfer(_auth):
    if 'username' in _auth and 'password' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            if _auth['action'] == 'EXPORT':
                print('export')
                emit('transfer', {'queries': sqlite.export_database()})
                emit('message', {'title': 'Export Data', 'text': 'Success!'})
            elif _auth['action'] == 'IMPORT':
                data = _auth['data']
                print(data)
                queries = data['queries']
                sqlite.import_query(queries)
                emit('message', {'title': 'Import Data', 'text': 'Success!'})
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})


@socketio.on('videos', namespace=socket_path)
def videos(_auth):
    if 'username' in _auth and 'password' in _auth and 'action' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            if _auth['action'] == 'GET' or _auth['action'] == 'DELETE':
                if _auth['action'] == 'DELETE':
                    file_name = _auth['file_name']
                    utils.delete_video(file_name)
                emit('video_list', {"videos": utils.get_video_list()})
            else:
                emit('error', {'error': 'Invalid Action'})
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})


@socketio.on('debug', namespace=socket_path)
def debug(_auth):
    if 'username' in _auth and 'password' in _auth and 'action' in _auth:
        if authenticate(_auth['username'], _auth['password']):
            if _auth['action'] == "INITTABLES":
                sqlite.init_database()
                emit('message', {'title': 'Init Tables', 'text': 'Success!'})
            elif _auth['action'] == "DROPTABLES":
                sqlite.drop_database()
                emit('message', {'title': 'Drop Tables', 'text': 'Success!'})
            elif _auth['action'] == "GETTABLES":
                emit('tables', {'tables': sqlite.get_table()})
                emit('message', {'title': 'Get Tables', 'text': 'Success!'})
            elif _auth['action'] == "INSERTDUMMY":
                sqlite.insert_dummy_data()
                emit('message', {'title': 'Insert Dummy Data', 'text': 'Success!'})
            elif _auth['action'] == "GETDATA":
                emit('data', {'data': sqlite.get_data()})
                emit('message', {'title': 'Get Data', 'text': 'Success!'})
            else:
                emit('error', {'error': 'Invalid Action'})
        else:
            print('unauthorized!')
            emit('error', {'error': 'Unauthorized'})
    else:
        print('Username and Password not found')
        emit('error', {'error': 'Username and Password are required!'})



# ---------------------------- SocketIO -----------------------------

if __name__ == '__main__':
    utils.create_folder_if_not_exists()
    socketio.run(app, host='0.0.0.0', port=50505)
