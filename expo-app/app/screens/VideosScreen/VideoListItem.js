import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, Text } from "react-native-elements";

const VideoListItem = ({ videoList, selectVideo, deleteVideo }) => {
  return (
    <>
      {videoList.map((item) => {
        // console.log(item);
        return (
          <View key={item.name} style={styles.videoItem}>
            {/* title */}
            <View style={styles.videoItemRow}>
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 1,
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    textAlign: "center",
                  }}
                >
                  {item.name}
                </Text>
              </View>
            </View>
            {/* detail */}
            <View style={styles.videoItemRow}>
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 2,
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    textAlign: "center",
                  }}
                >
                  {item.createAt}
                </Text>
              </View>
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 1,
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    textAlign: "center",
                  }}
                >
                  {item.size}
                </Text>
              </View>
            </View>
            {/* action */}
            <View style={styles.videoItemRow}>
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 1,
                }}
              >
                <Button
                  icon={{
                    name: "play-circle-outline",
                    color: "#737373",
                    size: 22,
                  }}
                  type="clear"
                  onPress={() => {
                    selectVideo(item.name);
                  }}
                />
              </View>
              <View
                style={{
                  ...styles.gridBorder,
                  flex: 1,
                }}
              >
                <Button
                  icon={{
                    name: "delete",
                    color: "#737373",
                    size: 22,
                  }}
                  type="clear"
                  onPress={() => {
                    deleteVideo(item.name);
                  }}
                />
              </View>
            </View>
          </View>
        );
      })}
    </>
  );
};

const styles = StyleSheet.create({
  videoItem: {
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  videoItemRow: {
    display: "flex",
    flexDirection: "row",
    marginVertical: 5,
  },
  gridBorder: {
    backgroundColor: "#EFEFEF",
    borderRadius: 20,
    paddingVertical: 5,
    marginHorizontal: 5,
  },
});

export default React.memo(VideoListItem, (prevProps, nextProps) => {
  return prevProps.videoList === nextProps.videoList;
});
